package ar.fiuba.tdd.tp1.cell.content;


import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;

import java.util.ArrayList;

public class Text implements CellContent {

    private String content;

    public Text(String string) {
        this.content = string;
    }

    @Override
    public double getValue() throws CellWithoutContent, ContentNotDouble {
        throw new ContentNotDouble();
    }

    @Override
    public String getValueAsString() throws CellWithoutContent {
        return content;
    }

    @Override
    public String getType() {
        return "String";
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public ArrayList getFormatt() {

        return null;
    }

    @Override
    public CellContent getCopy() {
        return new Text(this.content);
    }

}
