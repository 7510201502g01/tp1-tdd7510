package ar.fiuba.tdd.tp1.cell.content;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.cell.functions.GetNextImplementor;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;
import java.util.NoSuchElementException;


/*
 * This class represents the reference to another cell in a formula.
 * It is necessary because at the moment the formula is created, it has
 * no access to the real cell stored in the application. So this class
 * stores the reference and when possible access the real cell it needs.
 */
public class CellReference implements CellContent, GetNextImplementor {
    private Cell cell;
    private CellLocation cellLocation;

    private boolean firstTime;

    public CellReference(CellLocation cellLocation) {
        this.cellLocation = cellLocation;
        this.cell = null;

        this.firstTime = true;
    }

    private CellReference(CellLocation loc, Cell cell, boolean firstTime) {
        this.cellLocation = loc;
        this.cell = cell;

        this.firstTime = firstTime;
    }

    public double getValue() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            InvalidFormula, ContentNotDouble, InvalidCellLocationFormat {
        if (cell == null) {
            throw new IllegalArgumentException("Error: The cell reference wasn't completed before using it.");
        }
        return cell.getValue();
    }

    public String getValueAsString() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            InvalidFormula, ContentNotDouble, InvalidCellLocationFormat {
        return cell.getValueAsString();
    }

    //New object with the same atributes, otherwise it can be modified form outside.
    public CellLocation getCellLocation() {
        CellLocation cellLocation = new CellLocation(this.cellLocation);
        return cellLocation;
    }

    @Override
    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        this.cell = app.getCell(cellLocation);
    }



    @Override
    public boolean hasNext() {
        return (this.firstTime == true);
    }

    @Override
    public CellReference next() {
        if (!this.firstTime) {
            throw new NoSuchElementException();
        } else {
            this.firstTime = false;
            return this;
        }
    }

    @Override
    public void reset() {
        this.firstTime = true;
    }


    @Override
    public String getType() {
        return "Cell Reference";
    }

    @Override
    public String getContent() {
        return this.cellLocation.asString();
    }

    @Override
    public ArrayList<FormatInformation> getFormatt() {
        return null;
    }

    @Override
    public CellContent getCopy() {
        return new CellReference(this.cellLocation, this.cell, this.firstTime);
    }

}
