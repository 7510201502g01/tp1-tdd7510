package ar.fiuba.tdd.tp1.cell.functions;


import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;


public interface GetNextImplementor {

    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet;

    public boolean hasNext();

    public CellReference next() throws InvalidCellLocationFormat;

    public void reset();

}
