package ar.fiuba.tdd.tp1.cell.formatters;

/**
 * Created by milena on 29/10/15.
 */
public class  FormatInformation {
    private String formatter;


    FormatInformation(String formatter) {
        this.formatter = formatter;
    }

    public String getFormatter() {
        return formatter;
    }

}
