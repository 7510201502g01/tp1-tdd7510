package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;


public class Concat implements RangedOperation {
    StringBuilder stringBuilder = new StringBuilder();

    @Override
    public String getValueAsString(CellSet set) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        this.visitSet(set);
        return stringBuilder.toString();
    }

    @Override
    public double getValue(CellSet set) throws ContentNotDouble {
        throw new ContentNotDouble("Concat");
    }

    @Override
    public CellReference operate(CellReference ref1, CellReference ref2) throws CellWithoutContent,
            InvalidFormula, ContentNotDouble, NoSpreadsheetOpen, InvalidSheet, InvalidCellLocationFormat {
        stringBuilder.append(ref2.getValueAsString());
        return ref2;
    }

    @Override
    public double getResult() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, ContentNotDouble, InvalidFormula {
        throw new ContentNotDouble("Concat");
    }


    @Override
    public void setResult(CellReference ref) {
    }

}