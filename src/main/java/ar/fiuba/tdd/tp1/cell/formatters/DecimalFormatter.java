package ar.fiuba.tdd.tp1.cell.formatters;


import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

public class DecimalFormatter implements Formatter {

    private int numberOfDecimals;

    public DecimalFormatter() {
        this.numberOfDecimals = -1; // No format specified, uses default.
    }

    public DecimalFormatter(int numberOfDecimals) {
        if (numberOfDecimals >= 0) {
            this.numberOfDecimals = numberOfDecimals;
        } else {
            this.numberOfDecimals = -1;
        }
    }

    public String setFormat(String numberOfDecimals) throws InvalidFormatException {
        try {
            int decimals = Integer.parseInt(numberOfDecimals);
            if (decimals >= 0) {
                int aux = this.numberOfDecimals;
                this.numberOfDecimals = decimals;
                return Integer.toString(aux);
            } else {
                throw new InvalidFormatException("The quantity of decimals should be zero or positive.");
            }
        } catch (NumberFormatException ex) {
            throw new InvalidFormatException("The quantity of decimals should be integer.");
        }
    }

    public String getFormatted(String value) { //value se puede convertir a double sin problema
        double number = Double.parseDouble(value);
        if (this.numberOfDecimals == -1) { //Por default.
            if (number == (int) number) {
                return Integer.toString((int) number);
            } else {
                String result = String.format("%.2f", number);
                return result.replace(",",".");
            }
        } else {
            String result = String.format("%." + Integer.toString(this.numberOfDecimals) + "f", number);
            return result.replace(",",".");
        }
    }

    @Override
    public FormatInformation getFormat() {
        if (this.numberOfDecimals == -1) {
            return null;
        }
        return new FormatInformation(Integer.toString(this.numberOfDecimals));
    }

}
