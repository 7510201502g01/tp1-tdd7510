package ar.fiuba.tdd.tp1.cell.content;


import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;
import java.util.List;

/**
 * CellContent is an interface that encloses the expected.
 * behavior the Cell will have over its content, despite of its type.
 */
public interface CellContent {

    double getValue() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, InvalidFormula,
            ContentNotDouble, InvalidCellLocationFormat;

    default String getValueAsString() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return Double.toString(getValue());
    }

    default void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        return;
    }

    String getType();

    public default List<String> setFormat(String type, String format) throws InvalidFormatException {
        throw new InvalidFormatException("This type doesn't take formatters.");
    }

    String getContent();

    ArrayList<FormatInformation> getFormatt();


    CellContent getCopy();

    public default boolean hasCycle(ArrayList<CellLocation> list) {
        return false;
    }

}
