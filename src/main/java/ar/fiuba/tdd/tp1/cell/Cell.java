package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.cell.content.Empty;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;
import java.util.List;

/*
 * Cell represents an identifiable container for values the user may
 * want to store. It is identified by its location in the sheet and
 * its spreadsheet.
 */
public class Cell {

    CellContent actualContent;
    FactoryContent factory;
    CellLocation cellLocation;

    public Cell(CellLocation loc) {
        this.actualContent = new Empty();
        this.factory = new FactoryContent();
        this.cellLocation = loc;
    }

    public double getValue() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, InvalidFormula,
            ContentNotDouble, InvalidCellLocationFormat {
        return actualContent.getValue();
    }

    public String getValueAsString() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return actualContent.getValueAsString();
    }

    public boolean hasCycledReference() {
        ArrayList<CellLocation> list = new ArrayList<CellLocation>();
        list.add(this.cellLocation);
        return this.actualContent.hasCycle(list);
    }

    public CellContent getContent() {
        return this.actualContent;
    }

    /**
     * Writes the content received as a parameter.
     * Post: It replaces the existing _content
     **/
    /* Debería dejar de existir, en lo posible.*/
    public void setContent(CellContent content) {
        this.actualContent = content;
    }

    public void setContent(String content) {
        this.factory.createContent(content);
    }

    public CellContent setType(String type) throws NoSpreadsheetOpen, InvalidSheet, ContentNotWhatExpected,
            InvalidType, InvalidCellLocationFormat {
        CellContent oldContent = this.actualContent.getCopy();
        this.actualContent = factory.changeType(this.actualContent, type);
        return oldContent;
    }

    public List<String> setFormat(String type, String format) throws InvalidFormatException {
        return this.actualContent.setFormat(type, format);
    }

    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        this.actualContent.completeContent(app);
    }

    public ArrayList<FormatInformation> getFormatt() {
        return this.actualContent.getFormatt();

    }

    public String getContentAsString() {
        return this.actualContent.getContent();
    }


    public String getType() {
        return this.actualContent.getType();
    }

}
