package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

public interface RangedOperation {
// Interfaz para hacer lambdas para las Functions.

    CellReference result = null;

    public default double getValue(CellSet set) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        this.visitSet(set);
        return this.getResult();
    }

    public default void visitSet(CellSet set) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        CellReference result = null;
        set.reset();
        if (!set.hasNext()) {
            throw new InvalidFormula("Error: Function doesn't have a range to operate in.");
        }
        while (set.hasNext()) {
            CellReference auxiliar = set.next();
            result = this.operate(result, auxiliar);
        }

    }

    public default String getValueAsString(CellSet set) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        Double result = this.getValue(set);
        return result.toString();
    }

    public default CellReference operate(CellReference ref1, CellReference ref2) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, InvalidFormula, ContentNotDouble, InvalidCellLocationFormat {
        if (ref1 == null || compareValues(ref1.getValue(), ref2.getValue())) {
            this.setResult(ref2);
            return ref2;
        } else {
            this.setResult(ref1);
            return ref1;
        }
    }

    public default boolean compareValues(double value1, double value2) {
        return true;
    }

    public double getResult() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            ContentNotDouble, InvalidFormula, InvalidCellLocationFormat;

    public void setResult(CellReference ref);

}
