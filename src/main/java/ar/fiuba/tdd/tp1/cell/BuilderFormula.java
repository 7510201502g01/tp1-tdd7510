package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.cell.content.Formula;

import java.util.function.BinaryOperator;

/**
 * This class will be used by FactoryContent,
 * to delegate the creation of a Formula.
 * We were inspired by the builder pattern
 * because it is constructed term by term acumulatively
 * until finally we get a well constructed product.
 */
public class BuilderFormula {

    private Formula formula;

    public BuilderFormula() {
        this.formula = null;
    }

    public Formula getFormula() {
        if (formula == null) {
            return new Formula();
        } else {
            return formula;
        }
    }

    public void addOperand(CellContent op) {
        if (formula == null) {
            formula = new Formula().setFirstOperand(op);
        } else {
            if (formula.hasSecondOperand()) {
                Formula newTerm = new Formula().setSecondOperand(op);
                newTerm.setFirstOperand(formula);
                formula = newTerm;
            } else {
                formula.setSecondOperand(op);
            }
        }
    }

    public void addOperator(BinaryOperator<Double> op) {
        if (formula == null) {
            formula = new Formula().setOperator(op);
        } else {
            if (formula.hasOperand() || formula.hasSecondOperand()) {
                Formula newTerm = new Formula().setOperator(op);
                newTerm.setFirstOperand(formula);
                formula = newTerm;
            } else {
                formula.setOperator(op);
            }
        }
    }

}
