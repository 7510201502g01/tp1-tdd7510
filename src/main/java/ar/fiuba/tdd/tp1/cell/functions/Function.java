package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;

public class Function implements CellContent {

    CellSet set;
    RangedOperation operation;
    private final String content;

    public Function(RangedOperation op, CellSet set, String content) {
        this.operation = op;
        this.set = set;
        this.content = content;
    }

    @Override
    public double getValue() throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return operation.getValue(this.set);
    }

    @Override
    public String getValueAsString() throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return operation.getValueAsString(this.set);
    }

    @Override
    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        this.set.completeContent(app);
    }

    @Override
    public String getType() {
        return "Function";
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public ArrayList<FormatInformation> getFormatt() {

        return null;
    }

    @Override
    public CellContent getCopy() {
        return new Function(this.operation, this.set, this.content);
    }

}
