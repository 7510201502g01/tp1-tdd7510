package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

public class Max implements RangedOperation {

    CellReference result;

    public Max() {
        this.result = null;
    }

    @Override
    public boolean compareValues(double ref1, double ref2) {
        return ref1 < ref2;
    }

    @Override
    public double getResult() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return this.result.getValue();
    }

    @Override
    public void setResult(CellReference ref) {
        this.result = ref;
    }

}
