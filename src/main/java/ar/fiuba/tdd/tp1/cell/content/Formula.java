package ar.fiuba.tdd.tp1.cell.content;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.formatters.DecimalFormatter;
import ar.fiuba.tdd.tp1.exceptions.ApplicationException;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;
import java.util.function.BinaryOperator;

/*
 * Represents the content of a cell when it contains a formula, that
 * relates other cells with numbers through operations between them and
 * ther values, to obtain a final one.
 */
public class Formula implements CellContent {

    private CellContent function1;
    private CellContent function2;
    private BinaryOperator<Double> operator;
    private String formula;
    private DecimalFormatter formatter;

    public Formula() {
        this.function1 = null;
        this.function2 = null;
        this.operator = null;
        this.formatter = new DecimalFormatter();
    }

    private Formula(CellContent func1, CellContent func2, BinaryOperator<Double> op) {
        this.function1 = func1;
        this.function2 = func2;
        this.operator = op;
        this.formatter = new DecimalFormatter();
    }

    public Formula setOperator(BinaryOperator<Double> op) {
        this.operator = op;
        return this;
    }

    public Formula setFirstOperand(CellContent op1) {
        this.function1 = op1;
        return this;
    }

    public Formula setSecondOperand(CellContent op2) {
        this.function2 = op2;
        return this;
    }

    public boolean hasOperand() {
        return (operator != null);
    }

    public boolean hasFirstOperand() {
        return (function1 != null);
    }

    public boolean hasSecondOperand() {
        return (function2 != null);
    }

    @Override
    public double getValue() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, InvalidFormula,
            ContentNotDouble, InvalidCellLocationFormat {
        if (function1 == null) {
            throw new InvalidFormula("Error: Missing an operand in formula.");
        } else if (operator != null) {
            if (function2 != null) {
                return operator.apply(function1.getValue(), function2.getValue());
            } else {
                throw new InvalidFormula("Error: No second operand to operator.");
            }
        } else if (function2 != null) {
            throw new InvalidFormula("Error: No operator between operands.");
        } else {
            return function1.getValue();
        }
    }

    @Override
    public String getValueAsString() {
        String result;
        try {
            result = this.formatter.getFormatted(Double.toString(this.getValue()));
        } catch (ContentNotDouble e) {
            if (e.getMessage().equals("Concat")) {
                try {
                    result = this.function1.getValueAsString();
                } catch (ApplicationException e1) {
                    result = "Error: BAD_FORMULA";
                }
            } else {
                result = "Error: BAD_FORMULA";
            }
        } catch (ContentNotWhatExpected | NoSpreadsheetOpen | InvalidSheet | InvalidCellLocationFormat ex) {
            result = "Error:BAD_FORMULA";
        }
        return result;
    }

    @Override
    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        if (function1 != null) {
            function1.completeContent(app);
        }
        if (function2 != null) {
            function2.completeContent(app);
        }
    }

    @Override
    public String getType() {
        try {
            this.getValue();
            return "Number";
            //return "Number";
        } catch (ContentNotWhatExpected | NoSpreadsheetOpen | InvalidSheet | InvalidCellLocationFormat ex) {
            return "String";
        }
    }

    @Override
    public String getContent() {
        return this.formula;
    }

    public  void setFormulaString(String formula) {
        this.formula = formula;
    }

    public String getFormula() {
        return formula;
    }

    @Override
    public ArrayList getFormatt() {

        return null;
    }

    @Override
    public CellContent getCopy() {
        CellContent func1 = (this.function1 != null) ? this.function1.getCopy() : null;
        CellContent func2 = (this.function2 != null) ? this.function2.getCopy() : null;
        BinaryOperator<Double> op = this.operator;
        return new Formula(func1, func2, op);
    }
}
