package ar.fiuba.tdd.tp1.cell;


/*
 * CellLocation represents the collection of column, row and sheet
 * where the cell lies.
 * It encapsulates information that other parts of the program are going
 * to need to identify the cell they refer to; so it provides access to that information.
 */
public class CellLocation {

    String column;
    int row;
    String sheetName;

    public CellLocation(String sheetName, int row, String column) {

        this.column = column;
        this.sheetName = sheetName;
        this.row = row;

    }

    public CellLocation(CellLocation loc) {
        this.column = loc.column;
        this.sheetName = loc.sheetName;
        this.row = loc.row;
    }

    public String asString() {
        return this.sheetName + "." + Integer.toString(this.row) + "-" + this.column;
    }

    public String getSheet() {
        return this.sheetName;
    }

    public String getColumn() {
        return this.column;
    }

    public void setColumn(String value) {
        this.column = value;
    }

    public int getRow() {
        return this.row;
    }

    public void setRow(int value) {
        this.row = value;
    }

}
