package ar.fiuba.tdd.tp1.cell.functions;


import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class CellSet implements GetNextImplementor {

    ArrayList<GetNextImplementor> iterables;
    int index;
    GetNextImplementor actualIterator;

    public CellSet() {
        this.iterables = new ArrayList<>();
        this.index = 0;
        this.actualIterator = null;
    }

    public CellSet addItem(GetNextImplementor item) {
        this.iterables.add(item);
        return this;
    }

    @Override
    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        for (GetNextImplementor elem : this.iterables) {
            if (!(elem == null)) {
                elem.completeContent(app);
            }
        }
    }

    @Override
    public boolean hasNext() {
        if (this.actualIterator != null) {
            if (!this.actualIterator.hasNext()) {
                return !(this.index == this.iterables.size());
            } else {
                return true;
            }
        } else { //index = 0
            return (this.iterables.size() > 0);
        }
    }

    private void avanzarDeIterador() {
        this.actualIterator = this.iterables.get(this.index);
        this.actualIterator.reset();
        this.index++;
    }

    private CellReference checkValidIterator() throws InvalidCellLocationFormat {
        if (this.actualIterator != null) {
            return this.actualIterator.next();
        } else {
            throw new InvalidCellLocationFormat();
        }
    }

    private CellReference changeIterator() throws InvalidCellLocationFormat {
        if (this.index == this.iterables.size()) {
            throw new NoSuchElementException();
        } else {
            avanzarDeIterador();
            return checkValidIterator();
        }
    }

    @Override
    public CellReference next() throws InvalidCellLocationFormat {
        if (this.actualIterator != null) {
            if (!this.actualIterator.hasNext()) {
                return changeIterator();
            } else {
                return this.actualIterator.next();
            }
        } else if (index == 0) {
            if (this.iterables.size() > 0) {
                avanzarDeIterador();
                return checkValidIterator();
            } else {
                throw new NoSuchElementException();
            }
        } else {
            return changeIterator();
        }
    }

    @Override
    public void reset() {
        this.index = 0;
        this.actualIterator = null;
    }

}
