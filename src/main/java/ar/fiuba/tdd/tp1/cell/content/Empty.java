package ar.fiuba.tdd.tp1.cell.content;

import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;

import java.util.ArrayList;


/* Represents the lack of content in a cell. */
public class Empty implements CellContent {

    public Empty() {
    }

    @Override
    public double getValue() throws CellWithoutContent {
        throw new CellWithoutContent();
    }

    @Override
    public String getValueAsString() {
        return "No Value";
    }


    @Override
    public String getType() {
        return "No type";
    }

    @Override
    public String getContent() {
        return getValueAsString();
    }

    @Override
    public ArrayList getFormatt() {

        return null;
    }

    @Override
    public CellContent getCopy() {
        return new Empty();
    }

}
