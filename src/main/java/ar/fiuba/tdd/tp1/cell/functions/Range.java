package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.content.CellReference;

import java.util.NoSuchElementException;

public class Range implements GetNextImplementor {
    private final CellReference start;
    private final CellReference end;
    private Application app;
    private boolean firstMove = true;

    private CellReference cursor;

    public Range(CellReference start, CellReference end) {
        this.start = start;
        this.end = end;
        this.app = null;

        this.cursor = new CellReference(this.start.getCellLocation());
    }

    public void completeContent(Application app) {
        this.app = app;
    }


    @Override
    public void reset() {
        this.cursor = new CellReference(this.start.getCellLocation());
    }

    @Override
    public boolean hasNext() {
        String cursorColumn = this.cursor.getCellLocation().getColumn();
        String endColumn = this.end.getCellLocation().getColumn();

        //if i'm at the left--> true
        if (cursorColumn.compareTo(endColumn) < 0 || cursorColumn.length() < endColumn.length()) {
            return true;
        }
        if (cursorColumn.compareTo(endColumn) == 0) {
            if (this.cursor.getCellLocation().getRow() < this.end.getCellLocation().getRow()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public CellReference next() {


        if (this.hasNext()) {
            if (firstMove) {
                firstMove = false;
                try {
                    cursor = this.start;
                    start.completeContent(this.app);
                } catch (Exception ex) {
                    //throw new NoSuchElementException();
                }
                //ellReference current = moveOnePosition(cursor.getId());
                //cursor = current;
                return this.start;
            }

            CellReference current = moveOnePosition(cursor.getCellLocation());
            cursor = current;
            try {
                current.completeContent(this.app);
            } catch (Exception ex) {
                //Lo dejo asi para pasar los tests RangeTest
                //E ir incrementando funcionalidad de a poco.
                //throw new NoSuchElementException();
            }

            return current;
        }
        throw new NoSuchElementException();
    }

    private CellReference moveOnePosition(CellLocation actual) {
        if (haveToMoveColumn(actual)) {
            actual = moveColumn(actual);
        } else {
            actual = moveRow(actual);
        }
        return new CellReference(actual);
    }

    private CellLocation moveRow(CellLocation actual) {
        actual.setRow(actual.getRow() + 1);
        actual.setColumn(this.start.getCellLocation().getColumn());
        return actual;
    }

    boolean haveToMoveColumn(CellLocation actual) {
        return !(actual.getColumn().equals(this.end.getCellLocation().getColumn()));
    }

    private CellLocation moveColumn(CellLocation actual) {
        if (columnIsAllZ(actual.getColumn())) {
            StringBuilder allA = new StringBuilder();
            for (int i = 0; i <= actual.getColumn().length(); i++) {
                allA.append('a');
            }
            actual.setColumn(allA.toString());
        } else {
            actual.setColumn(simpleColumnChange(actual.getColumn()));
        }
        return actual;
    }

    // zzz...z ---> true
    private boolean columnIsAllZ(String text) {
        for (char c : text.toCharArray()) {
            if (c != 'z') {
                return false;
            }
        }
        return true;
    }

    //I already know that text matches: 'xxx....X[zzz]' ---> 'xxx....Y[aaa]'
    private String simpleColumnChange(String text) {
        char[] chars = text.toCharArray();
        int index = chars.length - 1;
        while (chars[index] == 'z') {
            index--;
        }
        chars[index]++;

        StringBuilder parcialResult = new StringBuilder();
        for (int i = 0; i <= index; i++) {
            parcialResult.append(chars[i]);
        }
        int tailLenght = chars.length - 1 - index;

        return addTailOfA(parcialResult.toString(), tailLenght);
    }

    private String addTailOfA(String result, int tailLenght) {
        StringBuilder buf = new StringBuilder();

        for (int i = 0; i < tailLenght; i++) {
            buf.append('a');
        }
        return result.concat(buf.toString());
    }

}
