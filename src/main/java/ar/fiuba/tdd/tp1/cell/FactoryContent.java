package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.IsDouble;
import ar.fiuba.tdd.tp1.cell.content.*;
import ar.fiuba.tdd.tp1.cell.content.Number;
import ar.fiuba.tdd.tp1.cell.functions.*;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Hashtable;
import java.util.Map;
import java.util.function.BinaryOperator;

/**
 * Concrete Creator of Factory Method.
 * This Factory creates content that can be a number or a formula only,
 * from the string the user indicated to be the value.
 * Raises an exception if it does not match any expected value.
 * For the formula creation, it delegates the work to the BuilderFormula.
 */
public class FactoryContent implements FactoryContentMethod {

    private static final char EQUALS_CHAR = '=';
    private Map<String, BinaryOperator<Double>> mapOperator;
    private Map<String, RangedOperation> mapFunctions;
    private Map<String, FactoryType> mapTypes;

    public FactoryContent() {
        mapOperator = new Hashtable<>();
        mapOperator.put("+", (number1, number2) -> number1 + number2);
        mapOperator.put("-", (number1, number2) -> number1 - number2);

        mapFunctions = new Hashtable<>();
        mapFunctions.put("max", new Max());
        mapFunctions.put("min", new Min());
        mapFunctions.put("average", new Average());
        mapFunctions.put("concat", new Concat());


        mapTypes = new Hashtable<>();
        mapTypes.put("string", createStringType());
        mapTypes.put("number", createNumberType());
        mapTypes.put("date", createDateType());
        mapTypes.put("currency", createMoneyType());
    }

    FactoryType createStringType() {
        return new FactoryType() {
            @Override
            public CellContent createContentFrom(CellContent oldContent) throws NoSpreadsheetOpen, InvalidSheet,
                    CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
                return new Text(oldContent.getValueAsString());
            }
        };
    }

    FactoryType createNumberType() {
        return new FactoryType() {
            @Override
            public CellContent createContentFrom(CellContent oldContent) throws NoSpreadsheetOpen, InvalidSheet,
                    CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
                return new Number(oldContent.getValue());
            }
        };
    }

    FactoryType createDateType() {
        return new FactoryType() {
            @Override
            public CellContent createContentFrom(CellContent oldContent) throws NoSpreadsheetOpen, InvalidSheet,
                    CellWithoutContent, ContentNotDouble, ContentNotDate, InvalidFormula, InvalidCellLocationFormat {
                String old = oldContent.getValueAsString();
                try {
                    String pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
                    LocalDate date = LocalDate.parse(old, formatter);
                    return new Date(date, oldContent.getContent());
                } catch (DateTimeParseException ex) {
                    throw new ContentNotDate();
                }
            }
        };
    }

    FactoryType createMoneyType() {
        return new FactoryType() {
            @Override
            public CellContent createContentFrom(CellContent oldContent) throws NoSpreadsheetOpen, InvalidSheet,
                    CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
                return new Money(oldContent.getValue());
            }
        };
    }

    /**
     * @param value Is a input String without preconditions.
     * @return Valid CellContent, with a Number, Formula, CellReference or Text.
     */
    @Override
    public final CellContent createContent(final String value) {
        CellContent content;
        if (value.length() > 0 && value.charAt(0) == EQUALS_CHAR) {
            String formula = value.substring(1);
            if (formula.equals("") || formula.matches("[ ]+")) {
                formula = "";
            }
            content = createFormula(formula, value);
           // content.setFormat(value);
        } else {
            content = createNonFormulaContent(value);
        }
        return content;
    }

    private Formula createFormula(String formula, String formulaString) {
        BuilderFormula builder = new BuilderFormula();
        String[] splittedFormula = formula.trim().split("[ ]+");
        int index = 0;
        while (index < splittedFormula.length) {
            if (mapOperator.containsKey(splittedFormula[index])) {
                builder.addOperator(mapOperator.get(splittedFormula[index]));
            } else {
                builder.addOperand(this.createFormulaContent(splittedFormula[index]));
            }
            index++;
        }
        Formula formula1 = builder.getFormula();
        formula1.setFormulaString(formulaString);
        return builder.getFormula();
    }

    private CellContent createRangedOperation(String value) {

        String function = value.substring(0, value.indexOf("(")).toLowerCase();
        String set = value.substring(value.indexOf("(") + 1, value.length() - 1);
        String[] ranges = set.split(",");
        CellSet setObject = new CellSet();
        for (String range : ranges) {
            //Si hay una referencia invalida guarda null.
            if (range.contains(":")) {
                Range rangeObject = new FactoryRange().build(range);
                setObject.addItem(rangeObject);
            } else {
                try {
                    CellReference ref = createCellReference(range);
                    setObject.addItem(ref);
                } catch (InvalidCellLocationFormat ex) {
                    setObject.addItem(null);
                }
            }
        }
        return new Function(mapFunctions.get(function), setObject, value);
    }

    private CellReference createCellReference(String reference) throws InvalidCellLocationFormat {
        CellLocation cellLocation = new CellLocationBuilder().build((reference));
        return new CellReference(cellLocation);
    }

    private CellContent createFormulaContent(final String value) {
        CellContent content;
        if (value.matches("[a-zA-Z]+\\(.*\\)")
                && mapFunctions.containsKey(value.substring(0, value.indexOf("(")).toLowerCase())) {
            content = createRangedOperation(value);
        } else {
            try {
                content = createCellReference(value);
            } catch (InvalidCellLocationFormat ex) {
                content = createNonFormulaContent(value);
            }
        }
        return content;
    }

    private CellContent createNonFormulaContent(final String value) {
        CellContent content;
        if (IsDouble.stringIsDouble(value)) {
            content = new Number(Double.parseDouble(value));
        } else {
            content = new Text(value);
        }
        return content;
    }

    public CellContent changeType(CellContent oldContent, String type) throws NoSpreadsheetOpen,
            InvalidSheet, InvalidType, ContentNotWhatExpected, InvalidCellLocationFormat {
        if (!mapTypes.containsKey(type.toLowerCase())) {
            throw new InvalidType();
        } else {
            return mapTypes.get(type.toLowerCase()).createContentFrom(oldContent);
        }
    }

}
