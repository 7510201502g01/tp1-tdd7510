package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;


public class Average implements RangedOperation {
    private double quantity;
    private double sum;

    public Average() {
        this.quantity = 0;
        this.sum = 0;
    }

    @Override
    public CellReference operate(CellReference ref1, CellReference ref2) throws CellWithoutContent,
            InvalidFormula, ContentNotDouble, NoSpreadsheetOpen, InvalidSheet, InvalidCellLocationFormat {
        this.quantity += ref2.getValue();
        this.sum++;
        return ref2;
    }

    @Override
    public double getResult() throws CellWithoutContent, ContentNotDouble, InvalidFormula {
        return quantity / sum;
    }

    @Override
    public void setResult(CellReference ref) {
    }

}