package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.CloseSpreadSheetCommand;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;

import java.util.List;

/**
 * Parser of the command that simulates the closure of a spreadsheet.
 * It doesn't receive any argument;
 */
public class CloseSpreadSheetParser implements Parser {
    private static final int PARAMETERS = 1;

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid {
        if (parameters.size() == PARAMETERS) {
            return new CloseSpreadSheetCommand();
        } else {
            throw new AmountOfArgumentsInvalid();
        }

    }
}
