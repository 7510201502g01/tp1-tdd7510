package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.CreateSheetCommand;

/**
 * Parser of the command that allows the client to create a sheet in the openes Spreadsheet.
 */
public class CreateSheetParser implements Parser {

    public Command createCommand(String parameter) {
        return new CreateSheetCommand(parameter);
    }

    @Override
    public String getParameters() {
        return "<SheetName>";
    }

}
