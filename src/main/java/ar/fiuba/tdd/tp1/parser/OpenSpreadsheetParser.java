package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.OpenSpreadSheetCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;

import java.util.List;

/**
 * Parser of the command that allows the client to open an already existing spreadsheet.
 * * It has to receive the name of the spreadsheet that tries to be open.
 */
public class OpenSpreadsheetParser implements Parser {

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid {
        if (parameters.size() != 2) {
            throw new AmountOfArgumentsInvalid();
        }

        return new OpenSpreadSheetCommand(parameters.get(1));
    }
}
