package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.SaveSpreadSheet;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;

/**
 * Created by milena on 28/10/15.
 */
public class SaveSpreadSheetParser implements Parser {

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid, InvalidCellLocationFormat {
        if (parameters.size() != 2) {
            throw new AmountOfArgumentsInvalid();
        }

        return new SaveSpreadSheet(parameters.get(1));
    }
}
