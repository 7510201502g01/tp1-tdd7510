package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidCommand;

import java.util.*;

/**
 * Class that keeps a register of the commands that can be used by the client.
 * It delegates the argument parsing of the input line to the corresponding parser.
 */
public class ParserDirector {

    private Map<String, Parser> existingCommands;

    public ParserDirector() {

        this.existingCommands = new HashMap<>();
        addSpreadsheetCommands();
        this.existingCommands.put("setcelltype", new SetCellTypeParser());
        this.existingCommands.put("undo", new UndoParser());
        this.existingCommands.put("redo", new RedoParser());
        otherCommands();
        this.existingCommands.put("savespreadsheet", new SaveSpreadSheetParser());
        this.existingCommands.put("showfile", new ShowFileParser());
        this.existingCommands.put("openfile", new OpenFileParser());



    }

    private void otherCommands() {

        this.existingCommands.put("getcellvalue", new GetCellValueParser());
        this.existingCommands.put("writecell", new WriteCellParser());
        this.existingCommands.put("setcellformat", new SetCellFormatParser());
        this.existingCommands.put("createsheet", new CreateSheetParser());

    }
    
    private void addSpreadsheetCommands() {
        this.existingCommands.put("createspreadsheet", new CreateSpreadSheetParser());
        this.existingCommands.put("openspreadsheet", new OpenSpreadsheetParser());
        this.existingCommands.put("closespreadsheet", new CloseSpreadSheetParser());
    }

    public String getValidCommands() {
        Iterator it = this.existingCommands.entrySet().iterator();
        StringJoiner sj = new StringJoiner(" ").add("");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            sj.add("   ").add(((String)pair.getKey()).toUpperCase()).add( ((Parser)pair.getValue()).getParameters()).add("\n");
        }
        return sj.toString();
    }


    /**
     * Delegates the responsibility of parsing an incoming expression to the respective parser.
     *
     * @param expression represents customer input
     * @return a command that encapsulates the action to be executed.
     * @throws InvalidCommand            if there is no parser configured.
     * @throws InvalidCellLocationFormat if there was a problem with a cell's location.
     * @throws AmountOfArgumentsInvalid  if there was a problem with the amount of arguments passed to the command.
     */
    public Command parseLine(String expression) throws InvalidCommand, InvalidCellLocationFormat, AmountOfArgumentsInvalid {

        if (expression == null || expression.equals("")) {
            throw new InvalidCommand();
        } else {

            //expression = expression.toLowerCase();
            List<String> tokens = Arrays.asList(expression.split("[ ]+"));
            Parser parser = this.existingCommands.get(tokens.get(0).toLowerCase());
            if (parser == null) {
                throw new InvalidCommand(this.getValidCommands());
            } else {
                return parser.parseLine(tokens);
            }
        }
    }

}
