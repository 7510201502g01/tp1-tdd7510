package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.SetCellTypeCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;

public class SetCellTypeParser implements Parser {

    private static final int NUMBER_OF_PARAMETERS = 3;
    private CellLocationBuilder builder = new CellLocationBuilder();

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid, InvalidCellLocationFormat {
        if (parameters.size() != NUMBER_OF_PARAMETERS) {
            throw new AmountOfArgumentsInvalid();
        }
        CellLocation cellLocation = builder.build(parameters.get(1));

        return new SetCellTypeCommand(cellLocation, parameters.get(2));
    }

    @Override
    public String getParameters() {
        return getCellLocationParameter() + " <Type>";
    }


}
