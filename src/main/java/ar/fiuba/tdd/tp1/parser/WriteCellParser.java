package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.FactoryContent;
import ar.fiuba.tdd.tp1.cell.FactoryContentMethod;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.WriteCellCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * Parser of the command that allows the user to write a cell.
 * It has to receive information about the cell's location and the value that
 * wants to be written down. It might fail if one of both is invalid.
 */
public class WriteCellParser implements Parser {

    private static final int NUMBER_OF_PARAMETERS = 3;
    private CellLocationBuilder builder = new CellLocationBuilder();

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid, InvalidCellLocationFormat {

        if (parameters.size() < NUMBER_OF_PARAMETERS) {
            throw new AmountOfArgumentsInvalid();
        }
        CellLocation cellLocation = builder.build(parameters.get(1));
        FactoryContentMethod factoryContentMethod = new FactoryContent();
        List<String> valueAux = parameters.subList(2, parameters.size());
        String value = valueAux.stream().collect(Collectors.joining(" "));
        CellContent cellContent = factoryContentMethod.createContent(value);

        return new WriteCellCommand(cellLocation, cellContent);
    }

    @Override
    public String getParameters() {
        return getCellLocationParameter() + " <newCellValue>";
    }


}
