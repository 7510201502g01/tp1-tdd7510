package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.SetCellFormatCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class SetCellFormatParser implements Parser {

    private CellLocationBuilder builder = new CellLocationBuilder();

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid, InvalidCellLocationFormat {
        if (parameters.size() < 4) {
            throw new AmountOfArgumentsInvalid();
        }
        CellLocation cellLocation = builder.build(parameters.get(1));

        StringJoiner sj = new StringJoiner(" ");
        for (int i = 3; i < parameters.size(); i++) {
            sj.add(parameters.get(i));
        }
        String format = sj.toString();

        return new SetCellFormatCommand(cellLocation, parameters.get(2), format);
    }

    @Override
    public String getParameters() {
        return getCellLocationParameter() + " <TypeOfFormat> <Format>";
    }


}
