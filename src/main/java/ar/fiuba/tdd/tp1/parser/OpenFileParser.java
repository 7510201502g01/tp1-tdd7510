package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.OpenFile;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;

/**
 * Created by milena on 29/10/15.
 */
public class OpenFileParser implements Parser {

    @Override
    public Command parseLine(List<String> parameters) throws InvalidCellLocationFormat, AmountOfArgumentsInvalid {

        if ( 2 != parameters.size()) {
            throw new AmountOfArgumentsInvalid();
        } else {
            return  new OpenFile(parameters.get(1));
        }
    }
}
