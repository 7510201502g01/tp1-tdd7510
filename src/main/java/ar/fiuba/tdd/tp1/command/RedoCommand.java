package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;

/**
 * Class the simulates the action of redoing last command undone.
 * May fail if there was no changeable action made and undone.
 */
public class RedoCommand implements Command {
    @Override
    public void execute(Application application) throws WrongSpreadsheetOrSheetUse, ContentNotWhatExpected,
            InvalidType, InvalidCellLocationFormat, InvalidFormatException {
        application.redo();
    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}
