package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SpreadSheetAlredyExists;

/**
 * Class that simulates the creation of a spreadsheet.
 * It could fail if there is already a spreadsheet created with the name specify by the client.
 */
public class CreateSpreadSheetCommand implements Command, Changeable {

    private String name;

    public CreateSpreadSheetCommand(String name) {
        this.name = name;
    }

    @Override
    public void execute(Application application) throws SpreadSheetAlredyExists {
        if (application.existsSpreadSheet(this.name)) {
            throw new SpreadSheetAlredyExists();
        }
        application.addSpreadSheet(this.name);
    }

    @Override
    public Changeable getChangeable() {
        return this;
    }

    @Override
    public void undo(Application application) {
        application.deleteSpreadSheet(this.name);

    }

    @Override
    public void redo(Application application) throws SpreadSheetAlredyExists {
        this.execute(application);

    }
}
