package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.application.Spreadsheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SheetAlredyExists;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;

/**
 * Class that simulates de creation of a sheet in a spreadsheet.
 * It might fail if there isn't any spreadsheet open or if a sheet with the name specify by the client
 * already exists in the spreadsheet.
 */
public class CreateSheetCommand implements Command, Changeable {

    private String nameSheet;

    public CreateSheetCommand(String name) {
        this.nameSheet = name;
    }

    @Override
    public void execute(Application application) throws NoSpreadsheetOpen, SheetAlredyExists {
        if (!application.isASpreadSheetOpen()) {
            throw new NoSpreadsheetOpen();
        }
        Spreadsheet spreadSheet = application.getWorkingSpreadSheet();
        boolean added = spreadSheet.addSheet(this.nameSheet);
        if (!added) {
            throw new SheetAlredyExists();
        }
    }

    @Override
    public Changeable getChangeable() {
        return this;
    }

    @Override
    public void undo(Application application) throws WrongSpreadsheetOrSheetUse {
        //todo: falta moverlo a un comando
        Spreadsheet spreadsheet = application.getWorkingSpreadSheet();
        spreadsheet.delete(this.nameSheet);
    }

    @Override
    public void redo(Application application) throws InvalidSheet, NoSpreadsheetOpen, SheetAlredyExists {
        this.execute(application);
    }
}
