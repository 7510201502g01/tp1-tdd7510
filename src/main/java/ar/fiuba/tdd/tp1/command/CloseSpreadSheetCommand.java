package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;

/**
 * Class that simulates the closure of a spreadsheet (close command).
 * It might fail if a spreadsheet wasn't open previously.
 */
public class CloseSpreadSheetCommand implements Command, Changeable {

    String nameWorkingSpreadSheet;

    @Override
    public void execute(Application application) throws NoSpreadsheetOpen {
        if (!application.isASpreadSheetOpen()) {
            throw new NoSpreadsheetOpen();
        }
        savePreviousState(application);
        application.closeSpreadSheet();
    }

    /**
     * Stores the previous state of the applicaction. In this particular case it stores the name
     * of the spreadsheet that have been closed.
     */
    private void savePreviousState(Application application) {
        this.nameWorkingSpreadSheet = application.getWorkingSpreadSheet().getName();

    }

    @Override
    public Changeable getChangeable() {
        return this;
    }

    @Override
    public void undo(Application application) throws WrongSpreadsheetOrSheetUse {
        OpenSpreadSheetCommand reverseCommand = new OpenSpreadSheetCommand(this.nameWorkingSpreadSheet);
        reverseCommand.execute(application);
    }

    @Override
    public void redo(Application application) throws NoSpreadsheetOpen {
        this.execute(application);
    }
}
