package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;

/**
 * Created by milena on 29/10/15.
 */
public class OpenFile implements Command{

    private String nameFile;

    public OpenFile(String nameFile) {
        this.nameFile = nameFile;
    }

    @Override
    public void execute(Application application) throws Exception {
        application.openFile(this.nameFile);
    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}

