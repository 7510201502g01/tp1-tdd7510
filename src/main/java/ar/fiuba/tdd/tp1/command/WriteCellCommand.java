package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.application.Spreadsheet;
import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;

/**
 * Class that simulates the action of writing a cell.
 * It might fail if the sheet indicated as part of the cell's location doesn't belong to the actual
 * spreadsheet open or if there isn't a spreadsheet currently open.
 */
public class WriteCellCommand implements Command, Changeable {

    private final CellLocation cellLocation;
    CellContent cellContent;
    CellContent previousContent;


    public WriteCellCommand(CellLocation location, CellContent cellContent) {
        this.cellLocation = location;
        this.cellContent = cellContent;
    }

    @Override
    public void execute(Application application) throws InvalidSheet, NoSpreadsheetOpen {
        Cell cell = application.getCell(cellLocation);
        previousContent = cell.getContent();
        this.cellContent.completeContent(application);
        cell.setContent(this.cellContent);
    }

    @Override
    public Changeable getChangeable() {
        return this;
    }

    @Override
    public void undo(Application application) throws InvalidSheet {
        Spreadsheet actualSpreadSheet = application.getWorkingSpreadSheet();
        actualSpreadSheet.write(this.cellLocation, this.previousContent);
    }

    @Override
    public void redo(Application application) throws InvalidSheet, NoSpreadsheetOpen {
        this.execute(application);
    }
}
