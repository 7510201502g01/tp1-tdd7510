package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.ProblemsWritingFile;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;

import java.io.IOException;

/**
 * Command pattern.
 * Interface that defines what any command should be able to do.
 */
public interface Command {
    void execute(Application application) throws WrongSpreadsheetOrSheetUse, ContentNotWhatExpected,
            InvalidType, InvalidCellLocationFormat, InvalidFormatException, IOException, ProblemsWritingFile, Exception;

    Changeable getChangeable();
}
