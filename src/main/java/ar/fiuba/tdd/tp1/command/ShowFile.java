package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.ProblemsWritingFile;

import java.io.IOException;

/**
 * Created by milena on 28/10/15.
 */
public class ShowFile implements Command {

    private final String fileName;

    @Override
    public void execute(Application application) throws  IOException, ProblemsWritingFile {
        application.getCSV(this.fileName);
    }

    public ShowFile(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}
