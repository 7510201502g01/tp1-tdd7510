package ar.fiuba.tdd.tp1.vision;

/**
 * Vision object that shows the result received to the user
 * using the console.
 */
public class ConsoleVision implements Vision {

    @Override
    public void show(String expression) {
        System.out.println(expression);
    }
}
