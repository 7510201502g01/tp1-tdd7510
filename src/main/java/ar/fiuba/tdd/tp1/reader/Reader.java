package ar.fiuba.tdd.tp1.reader;

/**
 * Interface that defines the behaviour any object that obtains
 * the user's "command request" should have.
 */
public interface Reader {
    String read();
}
