package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.reader.ConsoleReader;
import ar.fiuba.tdd.tp1.reader.Reader;
import ar.fiuba.tdd.tp1.vision.ConsoleVision;
import ar.fiuba.tdd.tp1.vision.Vision;

/*
 * Main class that communicates the three steps of the program:
 * 1. Obtaining requests from user.
 * 2. Processing request.
 * 3. Showing the result of the processing.
 */
public class Client {

    public static void main(String[] args) {

        Application app = new Application();
        Vision vision = new ConsoleVision();
        vision.show("Bienvenido\n");
        vision.show("En esta aplicación puede usar los siguientes comandos:");
        vision.show(app.getValidCommands());
        vision.show("Ingrese \"exit\" cuando haya terminado.");
        String command;
        Reader reader = new ConsoleReader();
        while (!(command = reader.read()).equals("exit")) {
            String result = app.process(command);
            vision.show(result);
        }
    }

}
