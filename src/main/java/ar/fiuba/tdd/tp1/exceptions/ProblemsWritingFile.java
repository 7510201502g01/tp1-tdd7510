package ar.fiuba.tdd.tp1.exceptions;

/**
 * Created by milena on 22/10/15.
 */
public class ProblemsWritingFile extends Exception {
    public ProblemsWritingFile() {
        super("Error: problems writing file.");
    }
}
