package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

/**
 * Exception thrown when the clients tries to create a sheet that already exists in the
 * spreadsheet.
 */
public class SheetAlredyExists extends WrongSpreadsheetOrSheetUse {
    public SheetAlredyExists() {
        super("Error: Sheet already exists.");
    }
}
