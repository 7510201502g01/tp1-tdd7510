package ar.fiuba.tdd.tp1.exceptions.wrong0content;

/**
 * Exception thrown when the formula the user tries to write to a cell is not valid,
 * because of lack of operands, or inexistent operators or invalid operands.
 */
public class InvalidFormula extends ContentNotWhatExpected {
    /**
     * @param explanation new tipe of exception.
     */
    public InvalidFormula(final String explanation) {
        super(explanation);
    }
}
