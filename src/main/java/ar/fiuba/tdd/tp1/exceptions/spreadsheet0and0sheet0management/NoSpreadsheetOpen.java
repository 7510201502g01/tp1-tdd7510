package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

/**
 * Exception thrown when the client tries to make a change on a spreadsheet when
 * there no one open.
 */
public class NoSpreadsheetOpen extends WrongSpreadsheetOrSheetUse {
    public NoSpreadsheetOpen() {
        super("Error: No Spreadsheet opened to operate.");
    }
}
