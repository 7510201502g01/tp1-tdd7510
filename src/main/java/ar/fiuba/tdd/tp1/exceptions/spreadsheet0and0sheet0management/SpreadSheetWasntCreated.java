package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

/**
 * Exception to be raise when the client tries to open a spreadsheet that hasn't been yet
 * created by him.
 */
public class SpreadSheetWasntCreated extends WrongSpreadsheetOrSheetUse {
    public SpreadSheetWasntCreated() {
        super("Error: the current spreadsheet was not yet created.");
    }
}
