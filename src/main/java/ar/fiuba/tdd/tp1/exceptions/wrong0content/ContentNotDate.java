package ar.fiuba.tdd.tp1.exceptions.wrong0content;


public class ContentNotDate extends ContentNotWhatExpected {
    public ContentNotDate() {
        super("Error: The content in specified cell can't be converted to date.");
    }
}
