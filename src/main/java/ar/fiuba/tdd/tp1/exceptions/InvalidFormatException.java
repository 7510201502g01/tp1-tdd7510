package ar.fiuba.tdd.tp1.exceptions;


public class InvalidFormatException extends ApplicationException {
    public InvalidFormatException(String message) {
        super(message);
    }
}
