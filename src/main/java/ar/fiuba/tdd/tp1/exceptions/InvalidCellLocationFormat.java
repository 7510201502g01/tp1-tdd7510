package ar.fiuba.tdd.tp1.exceptions;

/**
 * Exception that's thrown when the format of the cell's location in not valid.
 */
public class InvalidCellLocationFormat extends ApplicationException {
    public InvalidCellLocationFormat() {
        super("Error: Invalid location of cell format.");
    }
}
