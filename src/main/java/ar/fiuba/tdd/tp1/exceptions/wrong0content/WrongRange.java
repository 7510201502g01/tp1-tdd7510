package ar.fiuba.tdd.tp1.exceptions.wrong0content;

/**
 * Created by dani on 22/10/15.
 */
public class WrongRange extends ContentNotWhatExpected {
    public WrongRange() {
        super("Error: Range with invalid cell reference.");
    }
}
