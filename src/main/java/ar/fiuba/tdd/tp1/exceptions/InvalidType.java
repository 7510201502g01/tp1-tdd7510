package ar.fiuba.tdd.tp1.exceptions;


public class InvalidType extends ApplicationException {

    public InvalidType() {
        super("Error: Invalid type");
    }

}
