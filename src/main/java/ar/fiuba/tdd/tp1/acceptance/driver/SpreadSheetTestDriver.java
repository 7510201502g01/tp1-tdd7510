package ar.fiuba.tdd.tp1.acceptance.driver;

import java.util.List;

public interface SpreadSheetTestDriver {

    List<String> workBooksNames();

    void createNewWorkBookNamed(String name);

    void createNewWorkSheetNamed(String workbookName, String name);

    List<String> workSheetNamesFor(String workBookName);

    void setCellValue(String workBookName, String workSheetName, String cellId, String value);

    String getCellValueAsString(String workBookName, String workSheetName, String cellId);

    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId);

    void undo();

    void redo();

    void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format);

    void setCellType(String workBookName, String workSheetName, String cellId, String type);

    void persistWorkBook(String workBookName, String fileName);

    void reloadPersistedWorkBook(String fileName);

}