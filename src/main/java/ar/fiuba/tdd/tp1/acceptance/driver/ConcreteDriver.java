package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.IsDouble;
import ar.fiuba.tdd.tp1.application.Application;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

public class ConcreteDriver implements SpreadSheetTestDriver {

    Application app = new Application();

    private String transformValue(String value, String workSheet, TransformFunction function) {
        try {
            String pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate.parse(value, formatter);
            return value;
        } catch (DateTimeParseException ex) {
            String[] formula = value.split("[ ]+");
            StringJoiner finalValue = new StringJoiner(" ");
            for (String str : formula) {
                if (!(IsDouble.stringIsDouble(str) || positionOfFirstNumber(str) == -1)) {
                    finalValue.add(function.evaluate(str, workSheet));
                } else {
                    finalValue.add(str);
                }
            }
            return finalValue.toString();
        }
    }

    public String translateCellValue(String value, String worksheet) {
        return transformValue(value, worksheet, new TransformFunction() {
                    @Override
                    public String evaluate(String loc, String workSheet) {
                        String sheet;
                        String oldCellLoc;
                        if (!loc.contains(".")) {
                            if (workSheet != null) {
                                sheet = workSheet;
                            } else {
                                sheet = "default";
                            }
                            oldCellLoc = loc;
                        } else {
                            int dot = loc.indexOf(".");
                            sheet = loc.substring(1, dot);
                            //Porque la worksheet que no es la pasada por parámetro se escribe con "!" delante.
                            oldCellLoc = loc.substring(dot + 1);
                        }
                        if (!(loc.contains("AVERAGE") || loc.contains("MAX") || loc.contains("MIN") || loc.contains("CONCAT"))) {
                            return sheet + "." + translateCellLocation(oldCellLoc);
                        } else {
                            return translateCellLocation(oldCellLoc);
                        }
                    }
                }
        );
    }

    private String translateCellLocation(String loc) {
        if (normalRangedOperation(loc)) {
            return translateNormalOperation(loc);
        } else if (concatOperation(loc)) {
            return translateConcatOperation(loc);
        }
        int posNumber = positionOfFirstNumber(loc);
        return loc.substring(posNumber) + "-" + loc.substring(0, posNumber);
    }

    private boolean normalRangedOperation(String loc) {
        return (loc.contains("AVERAGE") || loc.contains("MAX") || loc.contains("MIN"));
    }

    private boolean concatOperation(String loc) {
        return loc.contains("CONCAT");
    }

    private String translateNormalOperation(String loc) {
        String command = loc.substring(0, loc.indexOf("("));
        command = command.toLowerCase();
        String first = loc.substring(loc.indexOf("(") + 1,  loc.indexOf(":"));
        String second = loc.substring(loc.indexOf(":") + 1,  loc.indexOf(")"));
        first = "default." + translateCellLocation(first);
        second = "default." + translateCellLocation(second);
        return command + "(" + first + ":" + second + ")";
    }

    private String translateConcatOperation(String loc) {
        String concatCommand = loc.substring(0, loc.indexOf("(")).toLowerCase();
        concatCommand = concatCommand.toLowerCase();
        String elements = loc.substring(loc.indexOf("(") + 1, loc.indexOf(")"));
        List<String> splittedElements = Arrays.asList(elements.split(","));

        String result = concatCommand.concat("(");
        StringBuilder sb = new StringBuilder();
        sb.append(concatCommand + "(");
        for (String str : splittedElements) {
            sb.append("default.");
            sb.append(translateCellLocation(str));
            sb.append(",");
        }
        result = sb.toString();
        //El ultimo tiene una coma de mas.
        result = result.substring(0, result.length() - 1) + ")";
        return result;
    }


    /*Devuelve -1 si no hay un número en el string.
     * De todas formas suponemos que este caso no se da.
     */
    private int positionOfFirstNumber(String string) {
        for (int i = 0; i < string.length(); i++) {
            char character = string.charAt(i);
            if (Character.isDigit(character)) {
                return i;
            }
        }
        return -1;
    }

    private String untranslateCellValue(String value, String worksheet) {
        return transformValue(value, worksheet, new TransformFunction() {
                    @Override
                    public String evaluate(String loc, String workSheet) {
                        int dot = loc.indexOf(".");
                        String sheet = loc.substring(0, dot);
                        int dash = loc.indexOf("-");
                        String number = loc.substring(dot + 1, dash);
                        String letter = loc.substring(dash + 1);
                        if (sheet.equals(workSheet)) {
                            return letter + number;
                        } else {
                            return "!" + sheet + "." + letter + number;
                        }
                    }
                }
        );
    }

    @Override
    public List<String> workBooksNames() {
        return app.getSpreadsheets();
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        app.process("createSpreadsheet " + name);
        app.process("openSpreadSheet " + name);
        app.process("createsheet default");
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        app.process("openspreadsheet " + workbookName);
        app.process("createsheet " + name);
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        app.process("openspreadsheet " + workBookName);
        return app.getWorkingSpreadSheet().getSheets();
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        app.process("openspreadsheet " + workBookName);
        String result = app.process("writeCell " + workSheetName + "." + translateCellLocation(cellId) + " "
                + translateCellValue(value, workSheetName));
        if (result.equals("Error: Specified Sheet of cell does not exist.")) {
            throw new UndeclaredWorkSheetException();
        } else if (!result.equals("Done")) {
            throw new BadFormulaException();
        }
    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        app.process("openspreadsheet " + workBookName);
        String result = app.process("getCellValue " + workSheetName + "." + translateCellLocation(cellId));
        if (result.equals("Error: Specified Sheet of cell does not exist.")) {
            throw new UndeclaredWorkSheetException();
        }
        return result;
    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId)
            throws NumberFormatException, BadReferenceException {
        String result = "";
        try {
            result = getCellValueAsString(workBookName, workSheetName, cellId);
            return Double.parseDouble(result);
        } catch (NumberFormatException ex) {
            throw new BadFormulaException();
        } catch (StackOverflowError err ) {
            throw new BadReferenceException();
        }
    }



    double alternativeDoubleFormat(String result) {
        System.out.println(result);
        List<String> tokens = Arrays.asList(result.split(","));
        if (tokens.size() != 2) {
            throw new BadFormulaException();
        }
        double integerPart = Double.parseDouble(tokens.get(0));
        double decimalPart = Double.parseDouble(tokens.get(1));

        double decimalMult = Math.pow(10, -tokens.get(1).length());
        decimalPart *= decimalMult;

        if (integerPart > 0) {
            return integerPart + decimalPart;
        } else {
            return integerPart - decimalPart;
        }
    }


    @Override
    public void undo() {
        app.process("undo");
    }

    @Override
    public void redo() {
        app.process("redo");
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        app.process("openspreadsheet " + workBookName);
        app.process("setCellFormat " + workSheetName + "." + translateCellLocation(cellId)
                + " " + formatter + " " + format);
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        app.process("openspreadsheet " + workBookName);
        app.process("setCelltype " + workSheetName + "." + translateCellLocation(cellId) + " " + type);
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        app.process("openspreadsheet " + workBookName);
        app.process("savespreadsheet " + fileName);
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        app.process("openfile " + fileName);

    }

    /* For avoiding duplicate code in CPD check */
    private interface TransformFunction {
        public String evaluate(String value, String worksheet);
    }

}