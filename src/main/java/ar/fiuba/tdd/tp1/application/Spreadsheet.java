package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.command.WriteCellCommand;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;
import ar.fiuba.tdd.tp1.parser.WriteCellParser;

import java.util.*;


/*
 * Spreadsheet represents the collection of sheets that make up a document.
 * It manages sheets and their cells, to give access to them or write on them.
 * It is identified by name.
 */
public class Spreadsheet {

    String name;
    Hashtable<String, Sheet> sheets;

    public Spreadsheet(String name) {
        this.name = name;
        this.sheets = new Hashtable<>();
    }

    public String getName() {
        return this.name;
    }

    public List<String> getSheets() {
        return new ArrayList<String>(sheets.keySet());
    }

    public void write(CellLocation cellLocation, CellContent cellContent) throws InvalidSheet {
        String nameSheet = cellLocation.getSheet();
        if (!this.hasSheet(nameSheet)) {
            throw new InvalidSheet();
        }
        Sheet sheet = getSheet(nameSheet);
        sheet.write(cellLocation, cellContent);
    }

    public boolean addSheet(String nameSheet) {
        if (this.hasSheet(nameSheet)) {
            return false;
        }
        this.sheets.put(nameSheet, new Sheet(nameSheet));
        return true;
    }

    public boolean hasSheet(String nameSheet) {
        return sheets.containsKey(nameSheet);
    }

    public Sheet getSheet(String nameSheet) {
        return sheets.get(nameSheet);
    }


    public void delete(String name) {
        sheets.remove(name);
    }

    private void checkSheet(String sheetName) throws InvalidSheet {
        if (!this.hasSheet(sheetName)) {
            throw new InvalidSheet();
        }
    }

    public Cell getCell(CellLocation loc) throws InvalidSheet {
        String nameSheet = loc.getSheet();
        checkSheet(nameSheet);
        Sheet sheet = getSheet(nameSheet);
        return sheet.getCell(loc);
    }

    public String getCellValueAsString(CellLocation loc) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat, BadReferenceException {
        String nameSheet = loc.getSheet();
        checkSheet(nameSheet);
        Sheet sheet = getSheet(nameSheet);
        if (!sheet.hasCycledReferences()) {
            return sheet.getCellValueAsString(loc);
        } else {
            throw new BadReferenceException();
        }
    }

    public CellContent setCellType(CellLocation loc, String type) throws NoSpreadsheetOpen,
            InvalidSheet, InvalidType, ContentNotWhatExpected, InvalidCellLocationFormat {
        checkSheet(loc.getSheet());
        Sheet sheet = getSheet(loc.getSheet());
        return sheet.setCellType(loc, type);
    }

    public List<String> setCellFormat(CellLocation loc, String type, String format) throws InvalidSheet, InvalidFormatException {
        checkSheet(loc.getSheet());
        Sheet sheet = getSheet(loc.getSheet());
        return sheet.setCellFormat(loc, type, format);
    }

    public SpreadSheetIterator getIterator() {
        return new SpreadSheetIterator(this.sheets.entrySet().iterator());
    }



//    public Iterator getIterator() {
//        return this.sheets.entrySet().iterator();
//    }

    public SpreadSheetInformation getInformation(String version)  {
        SpreadSheetInformation spreadSheetInformation = new SpreadSheetInformation(this.name, version);
        SpreadSheetIterator spreadSheetIterator = this.getIterator();

        while (spreadSheetIterator.hasNext()) {

            CellInformation cellInformation = spreadSheetIterator.next();
            if (cellInformation.getContent().equals("No value")) {
                continue;

            }
            spreadSheetInformation.addCellInformation(cellInformation);
        }

        return spreadSheetInformation;

    }

    public void load(SpreadSheetInformation spreadSheetInformation, Application application) throws Exception {
        Iterator iterator = spreadSheetInformation.getIterator();

        while (iterator.hasNext()) {
            CellInformation cellInformation = (CellInformation) iterator.next();
            String cellContent = cellInformation.getContent();
            //
            String sheetName = cellInformation.getSheet();
            if (!hasSheet(sheetName)) {
                addSheet(sheetName);
            }

            WriteCellParser writeCellParser = new WriteCellParser();

//            printInformation(cellInformation);

            WriteCellCommand writeCellCommand = (WriteCellCommand) writeCellParser.parseLine(
                    Arrays.asList("writecell", cellInformation.getId(), cellContent));
            writeCellCommand.execute(application);
            CellLocationBuilder cellLocationBuilder = new CellLocationBuilder();
            CellLocation cellLocation = cellLocationBuilder.build(cellInformation.getId());
//            setCellType(cellLocation, cellInformation.getType());
            List<FormatInformation> formats = cellInformation.getCellFormat();
            Iterator iter = formats.iterator();
            while (iter.hasNext()) {
                FormatInformation formatInformation = (FormatInformation) iter.next();
                this.setCellFormat(cellLocation, cellInformation.getType(), formatInformation.getFormatter());
            }

        }
    }

    private void printInformation(CellInformation cellInformation) {
        System.out.println("*****************************************************");
        System.out.println("CELL INFORMATION");
        System.out.println(cellInformation.getId());
        System.out.println(cellInformation.getContent());
        System.out.println(cellInformation.getSheet());
        System.out.println(cellInformation.getType());
        System.out.println(cellInformation.getCellFormat());
    }
}

