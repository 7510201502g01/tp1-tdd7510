package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;

/**
 * This class maintains a register of the commands that have been call by the client.
 * This characteristic allows the system to support undo and redo functionality.
 */
public class ChangeManager {

    private Node actualNode;
    private Node firstNode;

    ChangeManager() {

        this.firstNode = new Node();
        this.actualNode = this.firstNode;
    }

    /**
     * Adds a command that can supports undo/redo functionality, to the register of commands that have been pressed.
     *
     * @param changeable command that have been pressed.
     */
    public void addChangeable(Changeable changeable) {
        if (changeable == null) {
            return;
        }
        Node newNode = new Node(changeable);
        this.actualNode.right = newNode;
        newNode.left = this.actualNode;
        this.actualNode = newNode;
    }

    /**
     * Checks whether the undo function can be used by the client.
     *
     * @return true if it can be done or false otherwise.
     */
    private boolean canUndo() {

        return this.firstNode != this.actualNode;
    }

    /**
     * Checks whether the redo function can be used by the client.
     *
     * @return true if it can be done or false otherwise.
     */
    private boolean canRedo() {

        return this.actualNode.right != null;
    }


    public void undo(Application application) throws WrongSpreadsheetOrSheetUse, CellWithoutContent, InvalidFormatException {
        if (!canUndo()) {
            throw new IllegalArgumentException("Error: Nothing to undo.");
        }


        this.actualNode.changeableAction.undo(application);
        moveLeft();
    }

    private void moveLeft() {

        this.actualNode = this.actualNode.left;
    }

    private void moveRight() {

        this.actualNode = this.actualNode.right;
    }


    public void redo(Application application) throws WrongSpreadsheetOrSheetUse, InvalidType, InvalidFormatException,
            ContentNotWhatExpected, InvalidCellLocationFormat {
        if (!canRedo()) {
            throw new IllegalArgumentException("Error: Nothing to redo.");
        }
        moveRight();
        this.actualNode.changeableAction.redo(application);

    }


    static class Node {
        public Node right;
        public Node left;
        private Changeable changeableAction;

        Node() {
            changeableAction = null;
            right = null;
            left = null;

        }

        Node(Changeable changeable) {
            changeableAction = changeable;
            right = null;
            left = null;
        }

    }
}

