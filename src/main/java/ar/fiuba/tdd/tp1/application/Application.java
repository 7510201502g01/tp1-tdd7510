package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.exceptions.*;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;
import ar.fiuba.tdd.tp1.parser.ParserDirector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/* Administrative class which handles the spreadsheets and content. It works as a communication point
 * for the commands that were interpreted from the user's input.
 */
public class Application {

    private Spreadsheet workingSpreadSheet;
    private Map<String, Spreadsheet> spreadSheets;
    private ParserDirector director;
    private ChangeManager changeManager;
    private String result;

    public Application() {
        this.spreadSheets = new HashMap<>();
        this.workingSpreadSheet = null;
        this.director = new ParserDirector();
        this.changeManager = new ChangeManager();
        this.result = "Done";
    }

    public List<String> getSpreadsheets() {
        return new ArrayList<String>(spreadSheets.keySet());
    }

    public Spreadsheet getWorkingSpreadSheet() {
        return workingSpreadSheet;
    }

    public boolean openSpreadSheet(String spreadSheetName) {
        if (this.workingSpreadSheet != null) {
            return false;
        }
        if (!existsSpreadSheet(spreadSheetName)) {
            return false;
        }
        workingSpreadSheet = spreadSheets.get(spreadSheetName);
        return true;
    }

    public boolean isASpreadSheetOpen() {
        return (workingSpreadSheet != null);
    }

    public boolean addSpreadSheet(String spreadSheetName) {
        if (existsSpreadSheet(spreadSheetName)) {
            return false;
        }
        this.spreadSheets.put(spreadSheetName, new Spreadsheet(spreadSheetName));
        return true;
    }

    public boolean existsSpreadSheet(String spreadSheetName) {
        return (this.spreadSheets.containsKey(spreadSheetName));
    }

    public boolean closeSpreadSheet() {
        if (this.workingSpreadSheet == null) {
            return false;
        }
        this.workingSpreadSheet = null;
        return true;
    }

    public boolean deleteSpreadSheet(String name) {
        if (existsSpreadSheet(name)) {
            this.spreadSheets.remove(name);
            if (this.workingSpreadSheet != null && this.workingSpreadSheet.getName().equals(name)) {
                this.workingSpreadSheet = null;
            }
            return true;
        }
        return false;
    }

    /**
     * Keeps the central action of the application.
     * It delegates the parsing of the client's input to the ParseDirector. From there, they
     * have to be handled correctly the possibles exception that can be raised.
     * Once the command was created, it executes the action that it involves. From there, likewise, the possibles
     * exceptions raised have to be handled.
     *
     * @param expression client's input
     * @return string with the result produced by the intention of the client.
     */
    public String process(String expression) {
        this.result = "Done";
        Command command;
        try {
            command = this.director.parseLine(expression);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        try {
            command.execute(this);
            changeManager.addChangeable(command.getChangeable());
            return this.result;
        } catch (ApplicationException | IOException | IllegalArgumentException ex) {
            //El segundo y tercer tipo se usan en ciertos lugares.
            return ex.getMessage();
        } catch (ProblemsWritingFile problemsWritingFile) {
            return problemsWritingFile.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getValidCommands() {
        return director.getValidCommands();
    }

    public void redo() throws WrongSpreadsheetOrSheetUse, ContentNotWhatExpected, InvalidType,
            InvalidFormatException, InvalidCellLocationFormat {
        this.changeManager.redo(this);
    }

    public void undo() throws WrongSpreadsheetOrSheetUse, CellWithoutContent, InvalidFormatException {
        this.changeManager.undo(this);
    }

    public void addCommandtoChangeManager(Command command) {
        Changeable changeable = command.getChangeable();
        changeManager.addChangeable(changeable);
    }

    private void checkWorkingSpreadsheet() throws NoSpreadsheetOpen {
        if (!this.isASpreadSheetOpen()) {
            throw new NoSpreadsheetOpen();
        }
    }

    /* Esta debería dejar de existir en lo posible.*/
    public Cell getCell(CellLocation loc) throws NoSpreadsheetOpen, InvalidSheet {
        checkWorkingSpreadsheet();
        return this.workingSpreadSheet.getCell(loc);
    }

    public void showCellValue(CellLocation loc) throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent,
            ContentNotDouble, InvalidFormula, InvalidCellLocationFormat, BadReferenceException {
        checkWorkingSpreadsheet();
        this.result = this.workingSpreadSheet.getCellValueAsString(loc);
    }

    public String getResult() {
        return this.result;
    }

    public CellContent setCellType(CellLocation loc, String type) throws NoSpreadsheetOpen, InvalidType,
            InvalidSheet, ContentNotWhatExpected, InvalidCellLocationFormat {
        checkWorkingSpreadsheet();
        return this.workingSpreadSheet.setCellType(loc, type);
    }

    public List<String> setCellFormat(CellLocation loc, String type, String format) throws NoSpreadsheetOpen,
            InvalidSheet, InvalidFormatException {
        checkWorkingSpreadsheet();
        return this.workingSpreadSheet.setCellFormat(loc, type, format);
    }

    public void getCSV(final String fileName) throws ProblemsWritingFile {
        CSVManager csvManager = new CSVManager();
        csvManager.writeCSV(fileName, this.workingSpreadSheet);
    }

    public void writeJson(String fileName) throws NoSpreadsheetOpen, IOException {
        checkWorkingSpreadsheet();
        JsonManager jsonManager = JsonManager.getInstance();
        jsonManager.writeJson(this.workingSpreadSheet, fileName);
    }

    public void openFile(String fileName) throws Exception {
        JsonManager jsonManager = JsonManager.getInstance();
        jsonManager.readJson(this, fileName);
    }

    public void load(SpreadSheetInformation spreadSheetInformation) throws Exception {
        this.workingSpreadSheet = new Spreadsheet(spreadSheetInformation.getName());
        this.workingSpreadSheet.load(spreadSheetInformation, this);

    }
}
