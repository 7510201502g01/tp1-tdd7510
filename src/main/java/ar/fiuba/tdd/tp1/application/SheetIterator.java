package ar.fiuba.tdd.tp1.application;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by milena on 22/10/15.
 */
public class SheetIterator {


    private Iterator cellIter;

    SheetIterator(Iterator iterator) {
        this.cellIter = iterator;
    }

    public Boolean hasNext() {
        return this.cellIter.hasNext();
    }

    public Map.Entry next() {
        return ((Map.Entry) this.cellIter.next());
    }

}
