package ar.fiuba.tdd.tp1.translate0api0test;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DriverTranslateTest {

    private ConcreteDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new ConcreteDriver();
    }

    @Test
    public void translateInt() {
        assertEquals("3", testDriver.translateCellValue("3", "sheet"));
    }

    @Test
    public void translateDouble() {
        assertEquals("3.7", testDriver.translateCellValue("3.7", "sheet"));
    }

    @Test
    public void translateNegativeInt() {
        assertEquals("-3", testDriver.translateCellValue("-3", "sheet"));
    }

    @Test
    public void translateNegativeDouble() {
        assertEquals("-3.7", testDriver.translateCellValue("-3.7", "sheet"));
        assertEquals("-1000.50", testDriver.translateCellValue("-1000.50", "sheet"));
    }

}
