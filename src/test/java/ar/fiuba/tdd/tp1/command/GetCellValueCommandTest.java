package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.FactoryContent;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test created to prove the proper functionality of the OpenSpreadSheet-command.
 * It assumes the correct functionality of the cell's Location builder and
 * it assumes that the entry content is valid.
 */
public class GetCellValueCommandTest {

    Application application;
    GetCellValueCommand getCellValueCommand;
    WriteCellCommand writeCellCommand;
    CellLocationBuilder builder;

    @Before
    public void setUp() {
        application = new Application();
        getCellValueCommand = null;
        builder = new CellLocationBuilder();
    }

    private void writeCellCorrectExecutionNumberValue() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        CellContent cellContent = (new FactoryContent()).createContent("5");
        writeCellCommand = new WriteCellCommand(cellLocation, cellContent);

        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");

        application.getWorkingSpreadSheet().addSheet("S1");
        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), true);


        assertEquals(application.getCell(cellLocation).getValueAsString(), "No Value");
        writeCellCommand.execute(application);
        assertEquals(application.getCell(cellLocation).getValueAsString(), "5");
    }

    private void writeCellCorrectExecutionFormulaValue() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        CellContent cellContent = (new FactoryContent()).createContent("= S1.1-A + S1.3-B");
        writeCellCommand = new WriteCellCommand(cellLocation, cellContent);

        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        application.getWorkingSpreadSheet().addSheet("S1");

        writeCellCommand.execute(application);
    }

    @Test(expected = NoSpreadsheetOpen.class)
    public void spreadSheetisntOpen() throws Exception {
        getCellValueCommand = new GetCellValueCommand(builder.build("S1.1-A"));
        getCellValueCommand.execute(application);
    }

    @Test(expected = InvalidSheet.class)
    public void sheetInCellLocationDoesntExist() throws Exception {
        this.writeCellCorrectExecutionNumberValue();
        getCellValueCommand = new GetCellValueCommand(builder.build("S2.1-A"));
        getCellValueCommand.execute(application);
    }

    @Test
    public void correctExecutionNumberValue() throws Exception {
        this.writeCellCorrectExecutionNumberValue();
        getCellValueCommand = new GetCellValueCommand(builder.build("S1.3-A"));
        getCellValueCommand.execute(application);
        assertEquals(application.getCell(builder.build("S1.3-A")).getValueAsString(), "5");
        assertEquals(application.getResult(), "5");
    }

    /**
     * CellContent's cell referenced in the formula don't have any content.
     */
    @Ignore("FALLA")
    @Test(expected = ContentNotWhatExpected.class)
    public void incorrectExecutionFormulaValue() throws Exception {
        this.writeCellCorrectExecutionFormulaValue();
        getCellValueCommand = new GetCellValueCommand(builder.build("S1.3-A"));
        getCellValueCommand.execute(application);
    }

    @Ignore("FALLA")
    @Test
    public void correctExecutionFormulaValue() throws Exception {
        this.writeCellCorrectExecutionFormulaValue();
        CellLocation location1 = builder.build("S1.1-A");
        WriteCellCommand writeCellCommand = new WriteCellCommand(location1, (new FactoryContent()).createContent("2"));
        CellLocation location2 = builder.build("S1.3-B");
        WriteCellCommand writeCellCommand1 = new WriteCellCommand(location2, (new FactoryContent()).createContent("3"));
        writeCellCommand.execute(application);
        writeCellCommand1.execute(application);
        getCellValueCommand = new GetCellValueCommand(builder.build("S1.3-A"));
        getCellValueCommand.execute(application);
        System.out.println(application.getResult());
        assertEquals(application.getResult(), "5");
    }

}