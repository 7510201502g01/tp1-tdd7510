package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class UndoRedoTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver = new ConcreteDriver();


    private void commonHistory() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10");
        testDriver.setCellValue("tecnicas", "default", "A1", "5");
    }

    //@Ignore
    @Test
    public void undoValue() {
        commonHistory();

        testDriver.undo();
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    //@Ignore
    @Test
    public void redoValue() {
        commonHistory();

        testDriver.redo();
        assertEquals(5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    //@Ignore
    @Test
    public void undoThenRedoValue() {
        commonHistory();

        testDriver.undo();
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);

        testDriver.redo();
        assertEquals(5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    //@Ignore
    @Test
    public void actionClearsRedo() {
        commonHistory();

        testDriver.undo();
        testDriver.setCellValue("tecnicas", "default", "A1", "7");
        testDriver.redo();

        assertEquals(7, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    //@Ignore
    @Test
    public void undoSheetCreation() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 1");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 2");

        testDriver.undo();

        assertThat(testDriver.workSheetNamesFor("tecnicas"), hasItem("sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), not(hasItem("sheet 2")));
    }

    //@Ignore
    @Test
    public void redoSheetCreation() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 1");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 2");

        testDriver.undo();
        assertThat(testDriver.workSheetNamesFor("tecnicas"), hasItem("sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), not(hasItem("sheet 2")));

        testDriver.redo();
        assertThat(testDriver.workSheetNamesFor("tecnicas"), hasItem("sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), hasItem("sheet 2"));
    }
}
