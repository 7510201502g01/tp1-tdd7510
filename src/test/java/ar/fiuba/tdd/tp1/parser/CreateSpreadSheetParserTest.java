package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test created to prove the proper functionality of the CreateSpreadSheet-parser.
 */
public class CreateSpreadSheetParserTest {

    private List<String> inComing;
    private CreateSpreadSheetParser createSpreadSheetParser = new CreateSpreadSheetParser();

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void lessArguments() throws Exception {
        inComing = Arrays.asList("createSpreadSheet");
        createSpreadSheetParser.parseLine(inComing);
    }

    @Test
    public void moreArgumentsTakesThemAsTwo() throws Exception {
        inComing = Arrays.asList("createSpreadSheet", "arg1", "arg2");
        createSpreadSheetParser.parseLine(inComing);
    }

    @Test
    public void correctArguments() throws Exception {
        inComing = Arrays.asList("createSpreadSheet", "arg1");
        createSpreadSheetParser.parseLine(inComing);
    }

}