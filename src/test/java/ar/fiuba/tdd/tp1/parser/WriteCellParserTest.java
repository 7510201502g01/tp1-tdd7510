package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;

public class WriteCellParserTest {

    private WriteCellParser parser = new WriteCellParser();

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void parseTooLittleArguments() throws Exception {
        parser.parseLine(Arrays.asList("WriteCell", "arg1"));
    }

    @Test
    public void parseCorrectly() throws Exception {
        parser.parseLine(Arrays.asList("WriteCell", "Hoja1.5-aa", "=3"));
    }

}
