package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;


/**
 * Test created to prove the proper functionality of the Redo-parser.
 */
public class RedoParserTest {

    private List<String> inComing;
    private RedoParser redoParser = new RedoParser();


    @Test(expected = AmountOfArgumentsInvalid.class)
    public void moreArguments() throws Exception {
        inComing = Arrays.asList("Redo", "arg1", "arg2");
        redoParser.parseLine(inComing);
    }

    @Test
    public void correctArguments() throws Exception {
        inComing = Arrays.asList("Redo");
        redoParser.parseLine(inComing);
    }

}