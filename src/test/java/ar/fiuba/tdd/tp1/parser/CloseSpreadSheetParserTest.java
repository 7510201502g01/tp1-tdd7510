package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test created to prove the proper functionality of the CloseSpreadSheet-parser.
 */
public class CloseSpreadSheetParserTest {

    private List<String> inComing;
    private CloseSpreadSheetParser closeSpreadSheetParser = new CloseSpreadSheetParser();


    @Test(expected = AmountOfArgumentsInvalid.class)
    public void moreArguments() throws Exception {
        inComing = Arrays.asList("closeSpreadSheetParser", "arg1", "arg2");
        closeSpreadSheetParser.parseLine(inComing);
    }

    @Test
    public void correctArguments() throws Exception {
        inComing = Arrays.asList("closeSpreadSheetParser");
        closeSpreadSheetParser.parseLine(inComing);
    }


}