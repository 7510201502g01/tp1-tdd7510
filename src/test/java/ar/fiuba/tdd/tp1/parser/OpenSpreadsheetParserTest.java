package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test created to prove the proper functionality of the OpenSpreadSheet-parser.
 */
public class OpenSpreadsheetParserTest {

    private List<String> inComing;
    private OpenSpreadsheetParser openSpreadsheetParser = new OpenSpreadsheetParser();

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void lessArguments() throws Exception {
        inComing = Arrays.asList("OpenSpreadSheet");
        openSpreadsheetParser.parseLine(inComing);
    }

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void moreArguments() throws Exception {
        inComing = Arrays.asList("OpenSpreadSheet", "arg1", "arg2");
        openSpreadsheetParser.parseLine(inComing);
    }

    @Test
    public void correctArguments() throws Exception {
        inComing = Arrays.asList("OpenSpreadSheet", "arg1");
        openSpreadsheetParser.parseLine(inComing);
    }

}