package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test created to prove the correct functioning of the Undo-Parser.
 */
public class UndoParserTest {

    private List<String> inComing;
    private UndoParser undoParser = new UndoParser();


    @Test(expected = AmountOfArgumentsInvalid.class)
    public void moreArguments() throws Exception {
        inComing = Arrays.asList("Undo", "arg1", "arg2");
        undoParser.parseLine(inComing);
    }

    @Test
    public void correctArguments() throws Exception {
        inComing = Arrays.asList("Undo");
        undoParser.parseLine(inComing);
    }


}